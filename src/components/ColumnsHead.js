import React from 'react';
import { XIcon } from '@heroicons/react/outline';
import { sizes } from '../constants';

const ColumnsHead = ({
  coloums,
  colInputHandler,
  deleteHandler,
  colValues
}) => {
  return (
    <>
      {coloums.map((item, index) => (
        <th
          className=' px-6 py-4 text-left text-xs font-medium text-gray-500 uppercase tracking-wider'
          key={index}
        >
          <div className='flex items-center'>
            <select
              name={item}
              onChange={(e) => colInputHandler(e, item)}
              value={colValues[item] ? colValues[item] : ''}
              className=' outline-none px-4 border-[2px] border-gray-400'
            >
              <option value='' disabled>
                size
              </option>
              {sizes.map((s, i) => (
                <option value={s} key={i}>
                  {s}
                </option>
              ))}
            </select>
            <XIcon
              className='w-4 h-4 ml-2 cursor-pointer hover:text-gray-800'
              onClick={() => deleteHandler(item)}
            />
          </div>
        </th>
      ))}
    </>
  );
};
export default ColumnsHead;
