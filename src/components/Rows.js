import React from 'react';
import { XIcon } from '@heroicons/react/outline';
import { colors } from '../constants';

const Rows = ({
  rows,
  rowHeadValues,
  coloums,
  rowValues,
  RawInputHandler,
  deleteRawHandler,
  RawHeadInputHandler,
  rowValuesSum,
  colValuesSum,
  totalSum
}) => {
  return (
    <>
      {rows.map((item, index) => (
        <tr
          className={`border-b-2 border-gray-100 300 cursor-pointer`}
          key={index}
        >
          <td className='px-6 py-5 text-xs lg:text-sm text-gray-900'>
            <div className='flex items-center'>
              <XIcon
                className='w-4 h-4 mr-2 cursor-pointer hover:text-gray-800'
                onClick={() => deleteRawHandler(item)}
              />

              <select
                name={item}
                onChange={(e) => RawHeadInputHandler(e, item)}
                value={rowHeadValues[item] ? rowHeadValues[item] : ''}
                className=' outline-none px-4 border-[2px] uppercase font-sans border-gray-400'
              >
                <option value='' disabled>
                  color
                </option>
                {colors.map((c, i) => (
                  <option value={c} key={i}>
                    {c}
                  </option>
                ))}
              </select>
            </div>
          </td>
          {coloums.map((x, i) => (
            <td className='px-6 py-5 text-xs lg:text-sm text-gray-900' key={i}>
              <input
                type='number'
                className='outline-none border-b-[1px] border-gray-400'
                value={rowValues[`input${item}_${x}`] || ''}
                name={`input${item}_${x}`}
                onChange={RawInputHandler}
                placeholder='write numbers'
              />
            </td>
          ))}
          <td className='text-center text-xs lg:text-sm text-gray-900 px-8 font-bold'>
            <span className=' bg-gray-200 px-2 py-2'>{rowValuesSum(item)}</span>
          </td>
        </tr>
      ))}
      <tr>
        <th></th>
        {coloums.map((c, i) => (
          <td className='text-xs lg:text-sm text-gray-900 font-bold' key={i}>
            <div className='p-5 border-b-2 border-gray-100'>
              <span className=' bg-gray-200 px-2 py-2 '>{colValuesSum(c)}</span>
            </div>
          </td>
        ))}
        <td className='text-center text-xs lg:text-sm text-gray-900 px-8 font-bold'>
          <div className='p-5 border-b-2 border-gray-100'>
            <span className=' bg-gray-200 px-2 py-2 '>{totalSum()}</span>
          </div>
        </td>
      </tr>
    </>
  );
};

export default Rows;
