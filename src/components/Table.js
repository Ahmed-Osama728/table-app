import React, { useState } from 'react';
import ColumnsHead from './ColumnsHead';
import Rows from './Rows';

const Table = () => {
  const [coloums, setColoums] = useState([0]);
  const [rows, setRows] = useState([0]);
  const [colValues, setColValues] = useState({});
  const [rowHeadValues, setRowHeadValues] = useState({});
  const [rowValues, setRowValues] = useState({});
  const [isEmpty, setError] = useState(false);

  const deleteHandler = (col) => {
    let editedCols = coloums.filter((item) => item !== col);
    setColoums(editedCols);
    let newColValues = { ...colValues };
    delete newColValues[col];
    setColValues(newColValues);
    let newRows = { ...rowValues };
    for (let i = 0; i < rows.length; i++) {
      delete newRows[`input${rows[i]}_${col}`];
    }
    setRowValues(newRows);
  };
  const deleteRawHandler = (row) => {
    let editedRows = rows.filter((item) => item !== row);
    setRows(editedRows);
    let newHeadRows = { ...rowHeadValues };
    let newRows = { ...rowValues };
    delete newHeadRows[row];
    for (let i = 0; i < coloums.length; i++) {
      delete newRows[`input${row}_${coloums[i]}`];
    }
    setRowHeadValues(newHeadRows);
    setRowValues(newRows);
  };
  const addColHandler = () => {
    let editedCols = [...coloums];
    let colsLength = editedCols.length;
    let newId = 0;
    if (colsLength !== 0) {
      newId = editedCols[colsLength - 1] + 1;
    }
    editedCols.push(newId);
    setColoums(editedCols);
  };
  const addRowHandler = () => {
    let editedRows = [...rows];
    let rowsLength = editedRows.length;
    let newId = 0;
    if (rowsLength !== 0) {
      newId = editedRows[rowsLength - 1] + 1;
    }
    editedRows.push(newId);
    setRows(editedRows);
  };

  const colInputHandler = (e, i) => {
    setColValues({
      ...colValues,
      [e.target.name]: e.target.value
    });
  };
  const RawHeadInputHandler = (e, i) => {
    setRowHeadValues({
      ...rowHeadValues,
      [e.target.name]: e.target.value
    });
  };
  const RawInputHandler = (e) => {
    setRowValues({
      ...rowValues,
      [e.target.name]: e.target.value
    });
  };

  const rowValuesSum = (item) => {
    let sum = 0;
    for (let i = 0; i < coloums.length; i++) {
      sum += +rowValues[`input${item}_${coloums[i]}`];
    }
    if (isNaN(sum)) {
      return '';
    } else {
      return sum;
    }
  };

  const colValuesSum = (item) => {
    let sum = 0;
    for (let i = 0; i < rows.length; i++) {
      sum += +rowValues[`input${rows[i]}_${item}`];
    }
    if (isNaN(sum)) {
      return '';
    } else {
      return sum;
    }
  };

  const totalSum = () => {
    let sum = 0;
    for (let i = 0; i < coloums.length; i++) {
      for (let j = 0; j < rows.length; j++) {
        sum += +rowValues[`input${rows[j]}_${coloums[i]}`];
        console.log(rowValues);
      }
    }
    if (isNaN(sum)) {
      return '';
    } else {
      return sum;
    }
  };

  const SubmitHandler = () => {
    setError(false);
    let allRowsValues = { ...rowValues, ...rowHeadValues };
    allRowsValues = Object.values(allRowsValues);
    let allColValues = Object.values(colValues);
    if (
      allColValues.length !== coloums.length ||
      allRowsValues.length !== rows.length * (coloums.length + 1)
    ) {
      setError(true);
      return;
    }

    let answer = [];
    for (let i = 0; i < coloums.length; i++) {
      for (let j = 0; j < rows.length; j++) {
        answer.push({
          size: colValues[coloums[i]],
          color: rowHeadValues[rows[j]],
          quantity: rowValues[`input${rows[j]}_${coloums[i]}`]
        });
      }
    }
    console.log(answer);
  };

  return (
    <>
      <div className='flex flex-col mt-2 p-3'>
        <div className='overflow-hidden  px-5 pb-4 bg-gray-200'>
          <table className='min-w-full divide-y rounded-md border-4'>
            <thead className='bg-gray-50 w-full '>
              <tr>
                <th>
                  <div className='flex items-center justify-between'>
                    <button
                      className='px-7 pb-1 rounded-full flex items-center justify-center text-xl'
                      onClick={addRowHandler}
                    >
                      +
                    </button>
                    <button
                      className='px-2 pb-7  rounded-full flex items-center justify-center text-xl'
                      onClick={addColHandler}
                    >
                      +
                    </button>
                  </div>
                </th>
                <ColumnsHead
                  coloums={coloums}
                  colValues={colValues}
                  colInputHandler={colInputHandler}
                  deleteHandler={deleteHandler}
                />
                <th></th>
              </tr>
            </thead>
            <tbody className='bg-white w-full'>
              <Rows
                rows={rows}
                rowHeadValues={rowHeadValues}
                coloums={coloums}
                rowValues={rowValues}
                RawInputHandler={RawInputHandler}
                deleteRawHandler={deleteRawHandler}
                RawHeadInputHandler={RawHeadInputHandler}
                rowValuesSum={rowValuesSum}
                colValuesSum={colValuesSum}
                totalSum={totalSum}
              />
            </tbody>
          </table>
          <button
            className='px-3 pb-2 pt-1 mt-5 bg-teal-600 text-white rounded-md flex items-center justify-center text-base'
            onClick={SubmitHandler}
            disabled={rows.length === 0 || coloums.length === 0}
          >
            Submit
          </button>
          {isEmpty ? (
            <p className='pt-3 text-sm text-red-500 font-semibold'>
              Please enter the missing values
            </p>
          ) : (
            ''
          )}
        </div>
      </div>
    </>
  );
};

export default Table;
